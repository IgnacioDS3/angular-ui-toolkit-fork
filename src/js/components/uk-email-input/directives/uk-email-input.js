angular.module('uk-toolkit.components')
    .directive('ukEmailInput', function($Localization) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                name:'@',
                maxLength: '@',
                errorResource: '@'
            },
            transclude: false,
            templateUrl: 'uk-email-input/uk-email-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.email = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var focusActiveClass = "uk-focus-active";
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var isEmail = function(raw) {
                    //VALIDATE EMAIL
                    var value = (raw || '');
                    if (value.length === 0) {
                        return true;
                    }
                    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    return (regex.test(value));
                };


                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = isEmail(value);

                        // Only set the value if pass the validation!
                        $scope.ngModel = vm.isValidityValid ? value.toLowerCase() : null;
                    } else {
                      $scope.ngModel = null;
                      if (value === undefined) {
                        vm.viewValue = null;
                      }
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onFocus = function() {
                    $element.addClass(focusActiveClass);
                };
                vm.onBlur = function() {
                    $element.removeClass(focusActiveClass);
                    if ($scope.ngModel) {
                        vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);
                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });

            }
        };
    });
