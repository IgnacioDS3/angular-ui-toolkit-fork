angular.module('uk-toolkit.components')
    .directive('ukSelectInput', function($filter, $Localization, $interpolate, $compile) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                name: '@',
                errorResource: '@',
                itemCollection: '&',
                items: '=',
                emptyItemText: '@',
                loading: '@',
                loadingItemText: '@',
                ngChange: '&',
                itemText: '@', // Template for the "selected value" && "list value"
                ngDisabled: '=' // Flag used to determinate if the control must be disabled or not.
            },
            transclude: true,
            templateUrl: 'uk-select-input/uk-select-input.tpl.html',
            controller: function($scope, $element, $q, $timeout) {
                var itemsNode = $element.find("select-container");
                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isLoading: true,
                    isValidityValid: true
                };
                vm.name =$scope.name;

                var childScope = $scope.$new();

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var resolveItemsDefer = $q.defer();

                //Check Data if Array or Promise
                var collection = $scope.itemCollection();
                if (collection && angular.isArray(collection)) {
                    //Smooth Delay
                    var delay = $timeout(function() {
                        $timeout.cancel(delay);
                        resolveItemsDefer.resolve(collection);
                    }, 350);
                } else if (collection && collection.then) {
                    //is Promise
                    collection.then(function(collection) {
                        resolveItemsDefer.resolve(collection);
                    }, function(err) {
                        resolveItemsDefer.reject();
                    });
                } else if (collection) {
                    throw {
                        error: "ITEM_COLLECTION: ITEMS_MUST_BE_ARRAY_OR_PROMISE"
                    };
                } else {
                    vm.isLoading = false;
                }

                var fill = function(values) {
                    var fragment = [
                        '<select ng-model="selectValue" ng-disabled="ngDisabled" name={{vm.name}}  ng-change="vm.selectChange(selectValue)" ',
                        'ng-options="selecteItem.' + $scope.itemText + ' for selecteItem in items">',
                        '</select>'
                    ].join("");
                    childScope.items = values;
                    childScope.selectValue = {};

                    var compiledDOM = $compile(fragment)(childScope);
                    itemsNode.html('').append(compiledDOM);

                    vm.isLoading = false;

                };
                resolveItemsDefer.promise.then(fill);


                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.selectChange = function(item, unMute) {
                    if (item) {
                        //Load When change (Post Compilation)
                        vm.displayValue = $interpolate("{{" + $scope.itemText + "}}")(item);
                        $scope.ngModel = item; //SET THE ITEM IN THE MODEL

                        if (!unMute) {
                            if ($scope.ngChange) {
                                //CALL ON-CHANGE BIND
                                var delay = $timeout(function() {
                                    $timeout.cancel(delay);
                                    $scope.ngChange();
                                }, 0);

                            }

                        }
                    }
                };


                $scope.$watch("items", function(value) {
                    if (value) {


                        if (typeof _.find(value, $scope.ngModel) === 'undefined' || typeof $scope.ngModel === 'undefined') {

                            $scope.ngModel = null;
                            vm.displayValue = null;

                        }

                        fill(value);


                    }
                });

                $scope.$watch("loading", function(value) {

                    if (value !== undefined) {
                        if (value === "true") {

                            $scope.ngModel = null;
                            vm.displayValue = null;
                            vm.isLoading = true;
                            $scope.ngDisabled=true;

                        } else {
                            vm.isLoading = false;
                            $scope.ngDisabled=false;
                        }
                    }

                });

                $scope.$watch("ngModel", function(newValue, oldValue) {
                    if (newValue !== oldValue && newValue !== null && newValue !== undefined) {
                        if (childScope.selectValue !== newValue) {
                            vm.selectChange(newValue, true);
                            childScope.selectValue = newValue;
                        }
                    } else {

                        vm.displayValue = "";

                        $scope.ngModel = null;
                    }
                });

            }
        };
    });
