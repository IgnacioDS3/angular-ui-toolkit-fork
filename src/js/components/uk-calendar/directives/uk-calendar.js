/*jshint loopfunc:true */
angular.module('uk-toolkit.components')
    .directive('ukCalendar', function($filter, $locale) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                onChangeDate: '&',
                minDate: '=',
                maxDate: '=',
                startDate: '=',
                excludeWeekends: '=',
                excludeDays: '='
            },
            transclude: false,
            templateUrl: 'uk-calendar/uk-calendar.tpl.html',

            link: function(scope, element, attrs, ctrls, ngModel) {
                //Validator Step
                var ctrl = ctrls[0];
                ctrl.$validators.isDate = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };

                scope.$watch('ngModel', function(newValue, oldValue) {

                    if (newValue) {
                        scope.vm.setDate(newValue);
                    }
                }, true);

            },
            controller: function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true,
                    initializedComponent: false,
                    dayNames: $locale.DATETIME_FORMATS.SHORTDAY,
                    monthNames: $locale.DATETIME_FORMATS.MONTH
                };

                var parseDate = function(chosenDate) {
                    return new Date(chosenDate);
                };

                var getWeekMonths = function(date) {

                    if (!vm.initializedComponent) {

                        if ($scope.$parent.startDate) {
                            chosenDate = parseDate($scope.$parent.startDate);


                        } else if (!date) {

                            date = new Date();
                        }else{
                            chosenDate = parseDate(date);
                        }

                        vm.initializedComponent = true;


                    }
                    var month = date.getMonth();
                    var year = date.getFullYear();

                    var from = new Date(year, month, 1);
                    var to = new Date(new Date(year, month + 1, 1) - 1);

                    //We need to start on Monday (1) and end's on Sunday (0)
                    var calendarFrom = from;
                    var calendarTo = to;
                    while (calendarFrom.getDay() !== 1) {
                        calendarFrom.setDate(calendarFrom.getDate() - 1);
                    }
                    while (calendarTo.getDay() !== 0) {
                        calendarTo.setDate(calendarTo.getDate() + 1);
                    }

                    //Now .... we need to split the days in groups of week's (7 days)
                    //so.. start to building the "week's"
                    var weeks = [];
                    var currentWeek = [];
                    var untilTime = calendarTo.getTime();
                    while (calendarFrom.getTime() < untilTime) {
                        var isSelected = false;
                        if (chosenDate.getDate() === calendarFrom.getDate() &&
                            chosenDate.getMonth() === calendarFrom.getMonth() &&
                            chosenDate.getFullYear() === calendarFrom.getFullYear()) {
                            isSelected = true;
                        }
                        var muted = null;
                        var currentDDateFormatted = moment(calendarFrom.toLocaleDateString(), 'DD/MM/YYYY');

                        if ($scope.maxDate && $scope.minDate) {
                                var minDateFormatted = moment($scope.minDate.toLocaleDateString(), 'DD/MM/YYYY');
                                var maxDateFormatted = moment($scope.maxDate.toLocaleDateString(), 'DD/MM/YYYY');
                                if ((currentDDateFormatted < minDateFormatted) || (currentDDateFormatted > maxDateFormatted) || (calendarFrom.getMonth() !== month)) {
                                    muted = true;
                                }

                            }
                            else if ($scope.minDate && !$scope.maxDate) {

                                var minDDateFormatted = moment($scope.minDate.toLocaleDateString(), 'DD/MM/YYYY');
                                muted = ((minDDateFormatted > currentDDateFormatted) || ((calendarFrom.getMonth() !== month))) ? true : false;

                            }

                            else if ($scope.maxDate && !$scope.minDate) {

                                var maxDateFormatted2 = moment($scope.maxDate.toLocaleDateString(), 'DD/MM/YYYY');
                                muted = ((maxDateFormatted2 < currentDDateFormatted) || ((calendarFrom.getMonth() !== month))) ? true : false;

                            }
                            else {

                                muted = (calendarFrom.getMonth() !== month);

                            }


                        if ($scope.excludeWeekends) {
                            var numberDay = calendarFrom.getDay();
                            if (numberDay === 0 || numberDay === 6) {
                                muted = true;
                            }

                        }

                        if ($scope.excludeDays) {

                            angular.forEach($scope.excludeDays, function(data) {
                                if (isSameDate(data, calendarFrom)) {
                                    muted = true;

                                }
                            });

                        }

                        currentWeek.push({
                            date: new Date(calendarFrom),
                            muted: muted,
                            selected: isSelected
                        });

                        calendarFrom.setDate(calendarFrom.getDate() + 1);

                        if (calendarFrom.getDay() === 1) {
                            weeks.push(currentWeek);
                            currentWeek = [];
                        }
                    }
                    return weeks;
                };

                // ------------------------------
                // Private Method's
                // ------------------------------
                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = true;

                        $scope.ngModel = vm.isValidityValid ? value : null;
                    }
                };

                var isSameDate = function(firstDate, secondDate) {
                    return (firstDate.getFullYear() === secondDate.getFullYear() &&
                        firstDate.getMonth() === secondDate.getMonth() &&
                        firstDate.getDate() === secondDate.getDate());

                };

                   function render(selectedDate,source) {
                // Se agrega flag para condicionar renderizado del componente como tal
                // que tiene como objetivo iniciar el calendario en el mes correspondiente al minDate en caso de venir
                // De lo contrario este se inicializa en el mes de la fecha actual.
                // Diccionario de datos:
                //            MIN_DATE : Controla el evento del watcher asociado a @minDate
                //            INIT :     Controla el evento asociado al renderizado inicial del componente
                
                    var chosenDate = selectedDate.toISOString();

                    if ($scope.maxDate && $scope.minDate){

                        if(source==="MIN_DATE" || source==="INIT"){
                            chosenDate = parseDate(chosenDate);
                        } else {
                            chosenDate = parseDate(moment($scope.minDate.toLocaleDateString(), 'DD/MM/YYYY'));
                        }

                    } else { 

                        if(source === "MIN_DATE"){
                            chosenDate = parseDate(chosenDate);
                        } else {
                            chosenDate = new Date();
                        }

                    }

                    vm.currentDate = new Date(chosenDate);
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.years = [];

                    var currentYear = chosenDate.getFullYear();
                    for (var i = 0; i < 65; i++) {
                        vm.years.push(currentYear - i);
                    }
                }

                render(new Date(),"INIT");

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                vm.nextMonth = function() {
                    vm.setMonth(vm.currentDate.getMonth() + 1);
                };
                vm.previousMonth = function() {
                    vm.setMonth(vm.currentDate.getMonth() - 1);
                };
                vm.setMonth = function(month) {
                    month = (month) ? month : vm.currentDate.getMonth();
                    vm.currentDate.setMonth(month);
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.setYear = function(year) {
                    vm.currentDate.setFullYear(year);
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.setDate = function(newDate) {
                    var parsedDate = parseDate(newDate);
                    chosenDate = parsedDate;
                    vm.currentDate = parsedDate;
                    vm.currentMonthWeek = getWeekMonths(vm.currentDate);
                    vm.pickerYear = vm.currentDate.getFullYear();
                };
                vm.selectDate = function(date) {

                    writeValue(date);

                    var handler = $scope.onChangeDate();
                    if (handler) {
                        handler(date);
                    }
                };

                // ------------------------------
                // Event's
                // ------------------------------
                 $scope.$watch("minDate", function(value) {
                    if (value) {
                        render(value,"MIN_DATE");
                    }
                });
                $scope.$watch("maxDate", function(value) {
                    if (value) {
                        render(value,"MAX_DATE");
                    }
                });
            }
        };
    });
