angular.module('uk-toolkit.components')
    .directive('ukRutInput', function($filter, $Localization, $timeout) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                errorResource: '@',
                ngChange: '&'
            },
            transclude: false,
            templateUrl: 'uk-rut-input/uk-rut-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.rut = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var focusActiveClass = "uk-focus-active";
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true,
                    rutLength: 12
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var calculateDv = function(run) {
                    var M = 0;
                    var S = 1;
                    var T = run;
                    for (; T; T = Math.floor(T / 10)) {
                        S = (S + T % 10 * (9 - M++ % 6)) % 11;
                    }
                    return (S ? S - 1 : 'k').toString();
                };
                var isRut = function(raw) {
                    var rut = (raw || '');
                    if (rut.length === 0) {
                        return true;
                    }

                    if (rut && rut.toString().length < 7) {
                        return false;
                    }

                    var cleanRut = rut.replace(/\./ig, '').replace(/-/ig, '');
                    if (!(/^[0-9]+[0-9kK]{1}$/.test(cleanRut))) {
                        return false;
                    }

                    var dv = cleanRut.substring(cleanRut.length - 1, cleanRut.length).toLowerCase();
                    var run = cleanRut.substring(0, cleanRut.length - 1);

                    return (calculateDv(run) === dv.toString());
                };

                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {

                        vm.isValidityValid = isRut(value);
                        $scope.ngModel = vm.isValidityValid ? (value.replace(/\./ig, '').replace(/-/ig, '')).toUpperCase() : null;

                        if (vm.isValidityValid) {
                            vm.trigger("ngChange", value);
                        }
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.trigger = function(eventName) {
                    var delay = $timeout(function() {
                        $timeout.cancel(delay);

                        //call handler
                        $scope[eventName]();

                    }, 10);

                };

                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onFocus = function() {
                    vm.rutLength = 12;
                    $element.addClass(focusActiveClass);
                    if (vm.isValidityValid) {
                        vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onBlur = function(value) {
                    vm.rutLength = 12;
                    $element.removeClass(focusActiveClass);
                    if (vm.isValidityValid && $scope.ngModel) {

                        // Get DV And Run in parts
                        var fullRut = $scope.ngModel;
                        var dv = fullRut.substring(fullRut.length - 1, fullRut.length).toLowerCase();
                        var run = fullRut.substring(0, fullRut.length - 1);

                        vm.viewValue = "{0}-{1}".format([
                            $filter('number')(run, 0),
                            dv
                        ]);
                    }
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });
            }
        };
    });