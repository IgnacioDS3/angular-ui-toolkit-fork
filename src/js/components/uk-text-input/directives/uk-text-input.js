angular.module('uk-toolkit.components')
    .directive('ukTextInput', function($Localization,$compile) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                name:'@',
                restricted: '@',
                errorResource: '@', 
                maxLength  : '=', 
                patternModel : '@', 
                isLoading:'=',
                ngDisabled:'='
            },
            transclude: false,
            templateUrl: 'uk-text-input/uk-text-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                if (scope.patternModel) { 
                    elm.find("input").attr("ng-pattern", scope.patternModel); 
                    $compile(elm.contents())(scope); 
 
                } 
                ctrl.$validators.text = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: function($scope, $element, $q) {
                // ------------------------------
                // Model && default values
                // ------------------------------
                var focusActiveClass = "uk-focus-active";
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true
                };

                //Check if the resource exists!
                var locale = $Localization.exists($scope.errorResource);
                if (locale) {
                    vm.errorLabel = $Localization.get($scope.errorResource);
                }

                // ------------------------------
                // Private Method's
                // ------------------------------
                var isValidText = function(raw) {
                    //VALIDATE TEXT 
                    var value = (raw || '');
                     if (value.length === 0 || $scope.restricted === undefined) {
                        return true;
                    }else{

                         return (/^[A-Za-z\u00E0-\u00FC-' ]+$/i.test(value));
                    }
                };


                var writeValue = function(value) {
                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = isValidText(value);

                        // Only set the value if pass the validation!
                        $scope.ngModel = vm.isValidityValid ? value.toUpperCase() : null;
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                vm.onFocus = function() {
                    $element.addClass(focusActiveClass);
                };
                vm.onBlur = function() {
                    $element.removeClass(focusActiveClass);
                    vm.viewValue = $scope.ngModel;
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });

            }
        };
    });