angular.module('uk-toolkit.components')
    .directive('ukDatepickerInput', function($filter, $window) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                name:'@',
                dateFormat: '@',
                excludeWeekends: '=',
                excludeDays: '=',
                startDate: '=',
                minDate: '=',
                maxDate: '=',
                ngChange: '&', //Call when the date of the calendar changed!
                ngBlur: '&' //Call when the user out of the focus (BLUR)

            },
            transclude: false,
            templateUrl: 'uk-datepicker-input/uk-datepicker-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.isDate = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: function($scope, $element, $q) {
                var container = $element.find("material-input");
                var windowDOM = window.angular.element($window);
                var focusActiveClass = "uk-focus-active";

                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true,
                    checkFocus: false
                };



                vm.startDate = ($scope.startDate) ? $scope.startDate : null;
                vm.minDate = ($scope.minDate) ? $scope.minDate : null;
                vm.maxDate = ($scope.maxDate) ? $scope.maxDate : null;
                vm.excludeDays = ($scope.excludeDays) ? $scope.excludeDays : [];
                vm.excludeWeekends = ($scope.excludeWeekends) ? $scope.excludeWeekends : false;

                // ------------------------------
                // Private Method's
                // ------------------------------
                var writeValue = function(value) {
                    if ((!angular.isUndefined(value)) && value) {
                        var format = ($scope.dateFormat || 'dd-MM-yyyy');
                        //Se agrega lógica para que el model siempre una fecha válida o borrar el input en su defecto (null)
                        if (typeof value.getMonth !== 'function') {
                            var dateString = value.split("-");
                            var date = new Date(dateString[2] + "-" + dateString[1] + "-" + dateString[0]);
                            if (value.length === 10 && typeof date.getMonth === 'function') {
                                value = date;
                            } else {
                                value = null;
                            }
                        }

                        if (value !== null && value !== undefined) {
                            vm.isValidityValid = true;
                            var tomorrowDate = new Date();
                            $scope.ngModel = vm.isValidityValid ? value : null;
                            vm.viewValue = $filter('date')(value, format);
                        } else {
                            vm.viewValue = null;
                        }
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                var closeCalendar = function() {
                    container.removeClass("uk-open uk-datepicker-focus");
                };
                windowDOM.on('click', closeCalendar);

                vm.cancelPropagation = function(ev) {
                    ev.stopPropagation();
                    ev.preventDefault();
                };
                vm.onFocus = function() {
                    vm.checkFocus = true;
                    $element.addClass(focusActiveClass);
                    angular.element(document.querySelectorAll('.uk-open')).removeClass('uk-open uk-datepicker-focus');

                    container.addClass("uk-open uk-datepicker-focus ");

                    if (vm.isValidityValid) {
                        //vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onBlur = function() {
                    vm.checkFocus = true;
                    $element.removeClass(focusActiveClass);
                    if (vm.isValidityValid) {

                    }

                    var handler = $scope.ngBlur();
                    if (handler && vm.checkFocus) {
                        handler();
                    }
                    vm.checkFocus = false;
                };
                vm.onChangeDate = function(date) {

                    writeValue(date);

                    closeCalendar();

                    var handler = $scope.ngChange();
                    if (handler) {
                        handler(date);
                    }

                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });

                $scope.$watch("minDate", function(value) {
                    if (value) {
                        vm.minDate = value;
                        console.log("mindate picker", value);
                    }
                });

                $scope.$watch("maxDate", function(value) {
                    if (value) {
                        vm.maxDate = value;
                        
                    }
                });

                // ------------------------------
                // Destroy
                // ------------------------------
                $scope.$on("$destroy", function() {
                    windowDOM.off('click', closeCalendar);
                });
            }
        };
    });
